package main

import (
	"fmt"
	"os"
	"gitlab.com/peerity/chaincore-cli/cmd"
)

func main() {
	if err := cmd.RootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}


//package main
//
//import (
//	"gitlab.com/peerity/chaincore/types"
//	"strings"
//	"encoding/hex"
//	"log"
//	"encoding/json"
//	"golang.org/x/crypto/ripemd160"
//	//"github.com/tendermint/go-crypto"
//)
//
///**
//secret=             tuan
//_priKeyHex=         0123670CABF30801A3160EF30746EED381EC94C674E9CCBB004916B1465C6ED8DFEB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E
//PubKey Hex=         01EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E
//PubKey KeyString=   EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E
//PubKey Address=     E405128ABE228A095EFF8D4940C66EC7A40AAA91
//---------------------------------------------------------------------------
//secret=             jan
//_priKeyHex=         016A0AC0FD972C325D6CA5512B67A5E0AD996C4A3E9B59971D125164E6D4DB1A1CCDD6774218138DF657C7B0494894BBA596EB2ECCCC4C946D5ACEF3B5FCD2CE7B
//PubKey Hex=         01CDD6774218138DF657C7B0494894BBA596EB2ECCCC4C946D5ACEF3B5FCD2CE7B
//PubKey KeyString=   CDD6774218138DF657C7B0494894BBA596EB2ECCCC4C946D5ACEF3B5FCD2CE7B
//PubKey Address=     05D1D4B70CDA63A1A93FA381593A339BA9C67F19
// */
//
//func main() {
//	var tx types.Transaction
//
//	tx.Fee = 0
//	tx.Pubkey = "EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E"
//
//	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	//tx.Type = "SendTokenOperation"
//	//opt := types.SendTokenOperation {
//	//	ToAddress: "05D1D4B70CDA63A1A93FA381593A339BA9C67F19",
//	//	Amount: 10,
//	//}
//
//	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	//address, _ := hex.DecodeString("E405128ABE228A095EFF8D4940C66EC7A40AAA91")
//	//tx.Type = "GroupCreateOperation"
//	//opt := types.GroupCreateOperation {
//	//	Name: "Global",
//	//	Description: "Default",
//	//	Owner: address,
//	//}
//	//// group id
//	//hasher := ripemd160.New()
//	//hasher.Write([]byte(strings.ToUpper(opt.Name))) // does not error
//	//Id_bytes := hasher.Sum(nil)
//	//log.Println("Id", strings.ToUpper(hex.EncodeToString(Id_bytes)))
//
//
//	//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	//address, _ := hex.DecodeString("E405128ABE228A095EFF8D4940C66EC7A40AAA91")
//	//groupId, _ := hex.DecodeString("213BCCD3B8CF8A67910020FD782FAF99225A2145")
//	//
//	//tx.Type = "PostCreateOperation"
//	//opt := types.PostCreateOperation {
//	//	Title: "First Post",
//	//	Content: "This is the content of First Post",
//	//	Group: groupId,
//	//	Image: "https://www.norio.be/android-feature-graphic-generator/images/example.png",
//	//	Cat: []string {"peerity", "dev", "test"},
//	//	Location: "Hanoi, Vietnam",
//	//}
//	//
//	//// postId - B487ABDE757649C7FCF463111F61B5CE5CE2B1D9
//	//hasher := ripemd160.New()
//	//hasher.Write(address)
//	//hasher.Write([]byte(strings.ToUpper(opt.Title)))
//	//Id_bytes := hasher.Sum(nil)
//	//log.Println("Id", strings.ToUpper(hex.EncodeToString(Id_bytes)))
//
//
//	////////////////////////////////////////////////////////////////////////////////////////////////////////////
//	opt_arr, err := json.Marshal(opt)
//	if (err != nil) {
//		panic(err.Error())
//	}
//	log.Println("opt:", string(opt_arr))
//	opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))
//
//	tx.Operation = opt_str
//
//	// signature
//	prikey_bytes, _ := hex.DecodeString("23670CABF30801A3160EF30746EED381EC94C674E9CCBB004916B1465C6ED8DFEB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E");
//	prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)
//	sign := prikey.Sign([]byte(opt_str))
//	sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
//	sign_str = sign_str[2:len(sign_str)]
//
//	tx.Signature = sign_str
//
//	// encode to json
//	json_bytes, _ := json.Marshal(tx)
//
//	log.Println("tx=", string(json_bytes))
//
//	/**
//	{"Type":"SendTokenOperation","Operation":"7B22546F41646472657373223A2230354431443442373043444136334131413933464133383135393341333339424139433637463139222C22416D6F756E74223A31307D","Signature":"630D8971B678639C520EB7F44B22D4BB6CA2FC6D54A3A81B8192F99D75199CD7CFF3DBD61FB0C1D5C1F08950F41B11B00E215E4624F4285B64FF9E804E5CB90E","Pubkey":"EB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E","Fee":0}
//	 */
//}
//
