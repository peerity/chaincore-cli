package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
	rpcjson "github.com/gorilla/rpc/json"
	"golang.org/x/crypto/ripemd160"
)

type CreatePostFlagsType struct {
	Title 		string
	Content 	string
	Group 		string
	Cats 		string
	Location 	string
	PrivKey		string
	Username 	string
}

var createpostflags CreatePostFlagsType

func init() {
	createpostCmd.Flags().StringVarP(&createpostflags.Title, "Title", "", "", "Post Title")
	createpostCmd.Flags().StringVarP(&createpostflags.Content, "Content", "", "", "Post Content")
	createpostCmd.Flags().StringVarP(&createpostflags.Group, "Group", "", "", "Group ID in hex string")
	createpostCmd.Flags().StringVarP(&createpostflags.Cats, "Cats", "", "", "Category, separated by ,")
	createpostCmd.Flags().StringVarP(&createpostflags.Location, "Location", "", "", "Related Location")
	createpostCmd.Flags().StringVarP(&createpostflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	createpostCmd.Flags().StringVarP(&createpostflags.Username, "Username", "", "", "Username")

	RootCmd.AddCommand(createpostCmd)
}

/**
./chaincore-cli createpost --Title="Post 1" --Content="Test Content" --Group=C7D7C8A105F010C5490AEC87EEF6A326AE78AE1B  --PrivKey=7994986E9E892CD446AB87AA66C407A54BF27066BD4C735C031BFB05BB9131A1A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF --Username=abcd

{"method":"TransactionService.Broadcast","params":[{"Type":"PostCreateOperation","Operation":"7B225469746C65223A22506F73742031222C22436F6E74656E74223A225465737420436F6E74656E74222C2247726F7570223A22783966496F515877454D564A437579483776616A4A7135347268733D222C22496D616765223A22222C22436174223A5B22225D2C224C6F636174696F6E223A22227D","Signature":"34D1F0153B196610B37D74A40FA7057ADC40BDE1F6981A8584DA34DC8F2C60BB0A6C87A2D719E68384BC95DF769D7C047A9F359C4CED2FA2EEE349793823A20E","Username":"abcd","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":7663998747911059666}
2017/08/06 18:18:46 ID:  7C6A87AE160C818B64CE301C3267290DFC3D6134


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"method":"TransactionService.Broadcast","params":[{"Type":"PostCreateOperation","Operation":"7B225469746C65223A22506F73742031222C22436F6E74656E74223A225465737420436F6E74656E74222C2247726F7570223A22783966496F515877454D564A437579483776616A4A7135347268733D222C22496D616765223A22222C22436174223A5B22225D2C224C6F636174696F6E223A22227D","Signature":"34D1F0153B196610B37D74A40FA7057ADC40BDE1F6981A8584DA34DC8F2C60BB0A6C87A2D719E68384BC95DF769D7C047A9F359C4CED2FA2EEE349793823A20E","Username":"abcd","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":7663998747911059666}'


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"id": 1,"method": "PostService.GetPost","params": ["7C6A87AE160C818B64CE301C3267290DFC3D6134"]}'
{"result":{"ID":"7C6A87AE160C818B64CE301C3267290DFC3D6134","Title":"Post 1","Content":"Test Content","Author":"abcd","Group":"C7D7C8A105F010C5490AEC87EEF6A326AE78AE1B","Image":"","Cat":[""],"Location":""},"error":null,"id":1}


 */
var createpostCmd = &cobra.Command{
	Use:   "createpost",
	Short: "PostCreateOperation",
	Long:  `PostCreateOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(createpostflags.Title) <= 0 {
			fmt.Println("Error: Title is required")
			return
		}

		if len(createpostflags.Content) <= 0 {
			fmt.Println("Error: Content is required")
			return
		}

		if len(createpostflags.Group) <= 0 {
			fmt.Println("Error: Group is required")
			return
		}

		//if len(createpostflags.Cats) <= 0 {
		//	fmt.Println("Error: Cats is required")
		//	return
		//}
		//
		//if len(createpostflags.Location) <= 0 {
		//	fmt.Println("Error: Location is required")
		//	return
		//}

		if len(createpostflags.PrivKey) <= 0 {
			fmt.Println("Error: PrivKey is required")
			return
		}

		if len(createpostflags.Username) <= 0 {
			fmt.Println("Error: Username is required")
			return
		}

		prikey_bytes, _ := hex.DecodeString(createpostflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)



		var tx types.Transaction
		tx.Fee = 0
		tx.Username = createpostflags.Username
		tx.Pubkey = prikey.PubKey().KeyString()
		tx.Type = "PostCreateOperation"

		groupId, _ := hex.DecodeString(createpostflags.Group);

		opt := types.PostCreateOperation {
			Title: createpostflags.Title,
			Content: createpostflags.Content,
			Group: groupId,
			Image: "",
			Cat: strings.Split(createpostflags.Cats, ","),
			Location: createpostflags.Location,
		}

		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		//log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))

		// json-rpc
		buf, _ := rpcjson.EncodeClientRequest("TransactionService.Broadcast", tx)
		log.Println("jsonrpc= ", string(buf[:]) )


		// caculate id of this new post
		hasher := ripemd160.New()
		hasher.Write([]byte(createpostflags.Username))
		hasher.Write([]byte(strings.ToUpper(createpostflags.Title)))
		hash := hasher.Sum(nil)
		log.Println("ID: ", strings.ToUpper(hex.EncodeToString(hash)))
	},
}