package cmd

import (
	"github.com/spf13/cobra"
	"fmt"
	"math/rand"
	"github.com/tendermint/go-crypto"
	"strings"
	"encoding/hex"
)

type GenPrivKeyFlagsType struct {
	Passphrase string
	//Username string
}

var genprivkeyflags GenPrivKeyFlagsType

func init() {
	genprivkeyCmd.Flags().StringVarP(&genprivkeyflags.Passphrase, "Passphrase", "", "", "Passphrase")

	RootCmd.AddCommand(genprivkeyCmd)
}

/**
You could generate a random key by running:
./chaincore-cli genprivkey
Empty Passphrase, generate random Passphrase: xkfvPioCUPwqanTclVpPGyPUukpxFfkPlLtWuwuIvnqaFUkxFHokVxyQhNjyVDoz
PrivKey: 84A2D486B2E565C3847D66B4930AF56EC85C2C82CEF3A37AA11DABFF6060F6B92E754587849DE22942C2D30CF13C4FE816269B6AC4EB2F1C167BBB537A1E412C
PubKey: 2E754587849DE22942C2D30CF13C4FE816269B6AC4EB2F1C167BBB537A1E412C

or giving your Passphrase as:
./chaincore-cli genprivkey --Passphrase=abc
PrivKey: BA7816BF8F01CFEA414140DE5DAE2223B00361A396177A9CB410FF61F20015AD0BDF51005E5A525CC9BFB59F18070AF6A8A6013106530EB8944554A5AE4C3CFA
PubKey: 0BDF51005E5A525CC9BFB59F18070AF6A8A6013106530EB8944554A5AE4C3CFA
 */
var genprivkeyCmd = &cobra.Command{
	Use:   "genprivkey",
	Short: "Generate Private Key",
	Long:  `Generate Private Key`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(genprivkeyflags.Passphrase) <= 0 {
			genprivkeyflags.Passphrase = RandPassphrase()
			fmt.Println("Empty Passphrase, generate random Passphrase:", genprivkeyflags.Passphrase)
		}


		privKey := crypto.GenPrivKeyEd25519FromSecret([]byte(genprivkeyflags.Passphrase))

		fmt.Println("PrivKey:", strings.ToUpper(hex.EncodeToString(privKey.Bytes()[1:])))
		fmt.Println("PubKey: " + strings.ToUpper(privKey.PubKey().KeyString()))
	},
}

const randomPasspharseLength = 64
const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
func RandPassphrase() string {
	b := make([]byte, randomPasspharseLength)
	for i := range b {
		b[i] = letterBytes[rand.Int63() % int64(len(letterBytes))]
	}
	return string(b)
}