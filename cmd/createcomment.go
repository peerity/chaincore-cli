package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
	rpcjson "github.com/gorilla/rpc/json"
	"golang.org/x/crypto/ripemd160"
	"encoding/binary"
)

type CreateCommentFlagsType struct {
	PostID 		string 	// hex string
	Content 	string 	// hex string
	ParentComment 	string
	Nounce 		int64 	// any number like block heigh or unixtime or sequence number to avoid duplicated comment
	PrivKey		string
	Username 	string
}

var createcommentflags CreateCommentFlagsType

func init() {
	createcommentCmd.Flags().StringVarP(&createcommentflags.PostID, "PostID", "", "", "PostID in Hex String")
	createcommentCmd.Flags().StringVarP(&createcommentflags.Content, "Content", "", "", "Comment Content")
	createcommentCmd.Flags().StringVarP(&createcommentflags.ParentComment, "ParentComment", "", "", "ParentComment ID in hex string")
	createcommentCmd.Flags().Int64VarP(&createcommentflags.Nounce, "Nounce", "", 0, "any number like block heigh or unixtime or sequence number to avoid duplicated comment")
	createcommentCmd.Flags().StringVarP(&createcommentflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	createcommentCmd.Flags().StringVarP(&createcommentflags.Username, "Username", "", "", "Username")

	RootCmd.AddCommand(createcommentCmd)
}

/**
./chaincore-cli createcomment --PostID=C86E6ACCFCCA178ABE219A8CBF1518B4866E4D4F --Content="Test Comment" --ParentComment="" --Nounce=1 --PrivKey=7994986E9E892CD446AB87AA66C407A54BF27066BD4C735C031BFB05BB9131A1A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF --Username=a

2017/08/28 10:34:13 jsonrpc=  {"method":"TransactionService.Broadcast","params":[{"Type":"CommentCreateOperation","Operation":"7B22436F6E74656E74223A225465737420436F6D6D656E74222C22506F73744944223A22794735717A507A4B4634712B495A714D7678555974495A755455383D222C22506172656E74436F6D6D656E74223A22222C224E6F756E6365223A317D","Signature":"686619584E018F4FCB77BBA14EF3BDB8E5DB01856B327EEAD23FBD2FCD5C5EC0AF753C62247AD554A8080334E81FB1DF77C3EDA3F1390B9AA457D1D7B45B8F0C","Username":"a","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":5437063334514418070}
2017/08/28 10:34:13 ID:  9D4BFF0DA99A546F81767401CE0F26229FFE2F9A


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"method":"TransactionService.Broadcast","params":[{"Type":"CommentCreateOperation","Operation":"7B22436F6E74656E74223A225465737420436F6D6D656E74222C22506F73744944223A22794735717A507A4B4634712B495A714D7678555974495A755455383D222C22506172656E74436F6D6D656E74223A22222C224E6F756E6365223A317D","Signature":"686619584E018F4FCB77BBA14EF3BDB8E5DB01856B327EEAD23FBD2FCD5C5EC0AF753C62247AD554A8080334E81FB1DF77C3EDA3F1390B9AA457D1D7B45B8F0C","Username":"a","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":5437063334514418070}'


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"id": 1,"method": "CommentService.GetComment","params": ["9D4BFF0DA99A546F81767401CE0F26229FFE2F9A"]}'
{"result":{"ID":"9D4BFF0DA99A546F81767401CE0F26229FFE2F9A","PostID":"C86E6ACCFCCA178ABE219A8CBF1518B4866E4D4F","Parent":"","Content":"Test Comment","Author":"a"},"error":null,"id":1}

 */
var createcommentCmd = &cobra.Command{
	Use:   "createcomment",
	Short: "CommentCreateOperation",
	Long:  `CommentCreateOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(createcommentflags.PostID) <= 0 {
			fmt.Println("Error: PostID is required")
			return
		}

		if len(createcommentflags.Content) <= 0 {
			fmt.Println("Error: Content is required")
			return
		}

		if len(createcommentflags.PrivKey) <= 0 {
			fmt.Println("Error: PrivKey is required")
			return
		}

		if len(createcommentflags.Username) <= 0 {
			fmt.Println("Error: Username is required")
			return
		}

		prikey_bytes, _ := hex.DecodeString(createcommentflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)

		var tx types.Transaction
		tx.Fee = 0
		tx.Username = createcommentflags.Username
		tx.Pubkey = prikey.PubKey().KeyString()
		tx.Type = "CommentCreateOperation"

		PostID, _ := hex.DecodeString(createcommentflags.PostID);
		ParentComment, _ := hex.DecodeString(createcommentflags.ParentComment);

		opt := types.CommentCreateOperation {
			Content: createcommentflags.Content,
			PostID: PostID,
			ParentComment: ParentComment,
			Nounce: createcommentflags.Nounce,
		}

		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		//log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))

		// json-rpc
		buf, _ := rpcjson.EncodeClientRequest("TransactionService.Broadcast", tx)
		log.Println("jsonrpc= ", string(buf[:]) )


		// caculate id of this new comment
		hasher := ripemd160.New()
		hasher.Write(PostID)
		hasher.Write(ParentComment)
		hasher.Write([]byte(createcommentflags.Username))

		// http://stackoverflow.com/questions/35371385/how-can-i-convert-an-int64-into-a-byte-array-in-go
		Sequence_bytes := make([]byte, 8)
		binary.LittleEndian.PutUint64(Sequence_bytes, uint64(createcommentflags.Nounce))
		hasher.Write(Sequence_bytes)

		hash := hasher.Sum(nil)
		log.Println("ID: ", strings.ToUpper(hex.EncodeToString(hash)))
	},
}