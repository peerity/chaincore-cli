package cmd

import (
	"github.com/spf13/cobra"
	"log"
	//"fmt"
	//"gitlab.com/peerity/chaincore/types"
	//"encoding/json"
	//"golang.org/x/crypto/ripemd160"
	//"strings"
	//"encoding/hex"
)


type ProposalValidatorChangesFlagsType struct {
	Title string
	Content string
	PrivKey string
	ValidatorChanges  string
}

var proposalValidatorChangeslags ProposalValidatorChangesFlagsType

func init() {
	//proposalValidatorChanges.Flags().StringVarP(&proposalValidatorChangeslags.Title, "Title", "", "", "Proposal Title")
	//proposalValidatorChanges.Flags().StringVarP(&proposalValidatorChangeslags.Content, "Content", "", "", "Proposal Content")
	proposalValidatorChanges.Flags().StringVarP(&proposalValidatorChangeslags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	//proposalValidatorChanges.Flags().StringVarP(&proposalValidatorChangeslags.ValidatorChanges, "ValidatorChanges", "", "", "Diffs ValidatorChanges")
	RootCmd.AddCommand(proposalValidatorChanges)
}


/**
Only validator can create a proposal

Eg.,
./chaincore-cli proposalvalidatorchanges --Title="ProposalDataValidatorSetChange 2" --Content="adding E080EED089D81F7E937C750AE87D5ADC95B4EE8A" --PrivKey="23670CABF30801A3160EF30746EED381EC94C674E9CCBB004916B1465C6ED8DFEB3B42091EF6C2F8FA951319940C003BEC7AAE2336BD2AFABD6FB59EB4A3EF6E" --ValidatorChanges='[{"PubKey": "A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF", "Power": 1}]'
 */
var proposalValidatorChanges = &cobra.Command{
	Use:   "proposalvalidatorchanges",
	Short: "ProposalCreateOperation",
	Long:  `ProposalCreateOperation`,
	Run: func(cmd *cobra.Command, args []string) {

		log.Println("broken command, need to fix!")

		//log.Println("proposalvalidatorchanges")
		//
		//fmt.Println("Title=" + proposalValidatorChangeslags.Title)
		//fmt.Println("Content=" + proposalValidatorChangeslags.Content)
		//fmt.Println("PrivKey=" + proposalValidatorChangeslags.PrivKey)
		////fmt.Println("PubKey=" + flagPubKey)
		//fmt.Println("ValidatorChanges=" + proposalValidatorChangeslags.ValidatorChanges)
		//
		//diffs := make([]types.ValidatorChange,0)
		//json.Unmarshal([]byte(proposalValidatorChangeslags.ValidatorChanges), &diffs)
		//
		//fmt.Printf("%#v\n", diffs)
		//
		//prikey_bytes, _ := hex.DecodeString(proposalValidatorChangeslags.PrivKey);
		//prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)
		//
		//var tx types.Transaction
		//tx.Fee = 0
		//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//// ProposalCreateOperation
		//proposalDataValidatorSetChange := &types.ProposalDataValidatorSetChange {
		//	Diffs: diffs,
		//}
		//
		//proposalDataValidatorSetChange_json_bytes, err := json.Marshal(proposalDataValidatorSetChange)
		//if (err != nil) {
		//	panic(err.Error())
		//}
		//log.Println("proposalDataValidatorSetChange_json_bytes:", string(proposalDataValidatorSetChange_json_bytes))
		//proposalDataValidatorSetChange_json_hexstr := strings.ToUpper(hex.EncodeToString(proposalDataValidatorSetChange_json_bytes))
		//
		//tx.Type = "ProposalCreateOperation"
		//
		//voteState := make([]types.VoteState, 0)
		//
		////proposor_addr_bytes, _ := hex.DecodeString(prikey.PubKey().Address())
		//
		//voteState = append(voteState,
		//	types.VoteState {
		//		Addr: prikey.PubKey().Address(),
		//		Power: uint64(1),
		//		Point: -128,
		//	})
		//
		//
		//opt := types.ProposalCreateOperation {
		//	Type: "ProposalDataValidatorSetChange",
		//	Title: proposalValidatorChangeslags.Title,
		//	Content: proposalValidatorChangeslags.Content,
		//	Data: proposalDataValidatorSetChange_json_hexstr,
		//	Height: 0,
		//	Votes: voteState,
		//}
		//
		////
		//// proposal id
		//hasher := ripemd160.New()
		////hasher.Write(address)
		//hasher.Write([]byte(strings.ToUpper(opt.Title)))
		//Id_bytes := hasher.Sum(nil)
		//log.Println("Id", strings.ToUpper(hex.EncodeToString(Id_bytes)))
		//
		//////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//opt_arr, err := json.Marshal(opt)
		//if (err != nil) {
		//	panic(err.Error())
		//}
		//log.Println("opt:", string(opt_arr))
		//opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))
		//
		//tx.Operation = opt_str
		//
		//// signature
		//sign := prikey.Sign([]byte(opt_str))
		//sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		//sign_str = sign_str[2:len(sign_str)]
		//
		//tx.Signature = sign_str
		//
		//// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))
		//
		//// publishing transaction
	},
}