package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
	rpcjson "github.com/gorilla/rpc/json"
)

type AccountCreateFlagsType struct {
	PrivKey string
	Username string
}

var accountcreateflags AccountCreateFlagsType

func init() {
	accountcreateCmd.Flags().StringVarP(&accountcreateflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	accountcreateCmd.Flags().StringVarP(&accountcreateflags.Username, "Username", "", "", "Username")

	RootCmd.AddCommand(accountcreateCmd)
}

/**
 ./chaincore-cli accountcreate --PrivKey=7994986E9E892CD446AB87AA66C407A54BF27066BD4C735C031BFB05BB9131A1A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF --Username=abcd

 then we get tx in json-rpc format:
 {"method":"TransactionService.Broadcast","params":[{"Type":"AccountCreateOperation","Operation":"7B22557365726E616D65223A2261626364222C225075626B6579223A2241323738334431463038383338343241414336383630384432343038354237434337333031333532443943324331343743444637364545373446363537444146227D","Signature":"AAECEC9AFCA17F415E4CC218AC82436C6EC15C7ECAE7C2302E2106B413A433F4C7446AD6C4E5AA7FD53BDD939836CC1A1880B8716CAC9A2A8FF5617354A72D0B","Username":"abcd","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":2305121997126696319}

 next we will broadcast this transaction to rpc server ( a validator node )
 curl http://b.peerity.net:8088/rpc -X POST -H "Content-Type:application/json" --data '{"method":"TransactionService.Broadcast","params":[{"Type":"AccountCreateOperation","Operation":"7B22557365726E616D65223A2261626364222C225075626B6579223A2241323738334431463038383338343241414336383630384432343038354237434337333031333532443943324331343743444637364545373446363537444146227D","Signature":"AAECEC9AFCA17F415E4CC218AC82436C6EC15C7ECAE7C2302E2106B413A433F4C7446AD6C4E5AA7FD53BDD939836CC1A1880B8716CAC9A2A8FF5617354A72D0B","Username":"abcd","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":2305121997126696319}'

 */
var accountcreateCmd = &cobra.Command{
	Use:   "accountcreate",
	Short: "AccountCreateOperation",
	Long:  `AccountCreateOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		//log.Println("accountcreateCmd")
		//fmt.Println("PrivKey=" + accountcreateflags.PrivKey)
		//fmt.Println("PubKey=" + flagPubKey)


		if len(accountcreateflags.PrivKey) <= 0 {
			fmt.Println("Error: PrivKey is required")
			return
		}

		if len(accountcreateflags.Username) <= 0 {
			fmt.Println("Error: Username is required")
			return
		}

		prikey_bytes, _ := hex.DecodeString(accountcreateflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)



		var tx types.Transaction
		tx.Fee = 0
		tx.Username = accountcreateflags.Username

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// AccountCreateOperation
		tx.Pubkey = prikey.PubKey().KeyString()

		tx.Type = "AccountCreateOperation"
		opt := types.AccountCreateOperation {
			Username: accountcreateflags.Username,
			Pubkey: prikey.PubKey().KeyString(),
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		//log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))

		// json-rpc
		buf, _ := rpcjson.EncodeClientRequest("TransactionService.Broadcast", tx)
		log.Println("jsonrpc= ", string(buf[:]) )
	},
}