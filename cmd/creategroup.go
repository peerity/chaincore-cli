package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
	rpcjson "github.com/gorilla/rpc/json"
	"golang.org/x/crypto/ripemd160"
)

type CreateGroupFlagsType struct {
	Name 		string
	Description 	string
	PrivKey		string
	Username 	string
}

var creategroupflags CreateGroupFlagsType

func init() {
	creategroupCmd.Flags().StringVarP(&creategroupflags.Name, "Name", "", "", "Group Name")
	creategroupCmd.Flags().StringVarP(&creategroupflags.Description, "Description", "", "", "Group short Description")
	creategroupCmd.Flags().StringVarP(&creategroupflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	creategroupCmd.Flags().StringVarP(&creategroupflags.Username, "Username", "", "", "Username")

	RootCmd.AddCommand(creategroupCmd)
}

/**
./chaincore-cli creategroup --Name="Group Default" --Description="Test Group" --PrivKey=7994986E9E892CD446AB87AA66C407A54BF27066BD4C735C031BFB05BB9131A1A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF --Username=a


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"method":"TransactionService.Broadcast","params":[{"Type":"GroupCreateOperation","Operation":"7B224E616D65223A2247726F75702044656661756C74222C224465736372697074696F6E223A22546573742047726F7570222C224F776E6572223A2261626364222C225265717565737473223A6E756C6C7D","Signature":"54C15917AB7905EC6C06C83FA9197A83A69A442F33C4EF29E7A081FC237F936778BA8D98A9FDA0669CA171A6FEC129AA64967BD7388BA83C63BE0299523D7D00","Username":"abcd","Pubkey":"A2783D1F0883842AAC68608D24085B7CC7301352D9C2C147CDF76EE74F657DAF","Fee":0}],"id":7497058122959362492}'


curl http://localhost:8088/rpc -X POST -H "Content-Type:application/json" --data '{"id": 1,"method": "GroupService.GetGroup","params": ["C7D7C8A105F010C5490AEC87EEF6A326AE78AE1B"]}'

 */
var creategroupCmd = &cobra.Command{
	Use:   "creategroup",
	Short: "GroupCreateOperation",
	Long:  `GroupCreateOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(creategroupflags.PrivKey) <= 0 {
			fmt.Println("Error: PrivKey is required")
			return
		}

		if len(creategroupflags.Username) <= 0 {
			fmt.Println("Error: Username is required")
			return
		}

		if len(creategroupflags.Name) <= 0 {
			fmt.Println("Error: Name is required")
			return
		}

		if len(creategroupflags.Description) <= 0 {
			fmt.Println("Error: Description is required")
			return
		}

		prikey_bytes, _ := hex.DecodeString(creategroupflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)



		var tx types.Transaction
		tx.Fee = 0
		tx.Username = creategroupflags.Username
		tx.Pubkey = prikey.PubKey().KeyString()
		tx.Type = "GroupCreateOperation"

		opt := types.GroupCreateOperation {
			Name: creategroupflags.Name, // must be unique, so the Key is unique
			Description: creategroupflags.Description,
			Owner: creategroupflags.Username,
		}

		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		//log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))

		// json-rpc
		buf, _ := rpcjson.EncodeClientRequest("TransactionService.Broadcast", tx)
		log.Println("jsonrpc= ", string(buf[:]) )


		// caculate id of this new group
		hasher := ripemd160.New()
		hasher.Write([]byte(strings.ToUpper(creategroupflags.Name))) // does not error
		hash := hasher.Sum(nil)
		log.Println("ID: ", strings.ToUpper(hex.EncodeToString(hash)))
	},
}