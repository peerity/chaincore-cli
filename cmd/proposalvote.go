package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
)


type ProposalVoteFlagsType struct {
	ProposalKey string
	Vote int		// 0: no, 1: vote for both or nothing, 2: yes
	PrivKey string
}

var proposalvoteflags ProposalVoteFlagsType

func init() {
	proposalvoteCmd.Flags().StringVarP(&proposalvoteflags.ProposalKey, "ProposalKey", "", "", "ProposalKey")
	proposalvoteCmd.Flags().IntVarP(&proposalvoteflags.Vote, "Vote", "", 2, "Vote")
	proposalvoteCmd.Flags().StringVarP(&proposalvoteflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")

	RootCmd.AddCommand(proposalvoteCmd)
}

var proposalvoteCmd = &cobra.Command{
	Use:   "proposalvote",
	Short: "ProposalVoteOperation",
	Long:  `ProposalVoteOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		log.Println("proposalvote")
		fmt.Println("PrivKey=" + proposalvoteflags.PrivKey)
		//fmt.Println("PubKey=" + flagPubKey)


		prikey_bytes, _ := hex.DecodeString(proposalvoteflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)


		var tx types.Transaction
		tx.Fee = 0
		tx.Pubkey = prikey.PubKey().KeyString()

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// ProposalVoteOperation

		tx.Type = "ProposalVoteOperation"
		opt := types.ProposalVoteOperation {
			ProposalKey: proposalvoteflags.ProposalKey,
			Vote: int8(proposalvoteflags.Vote),
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		json_bytes, _ := json.Marshal(tx)
		log.Println("tx=", string(json_bytes))

		// publishing transaction
	},
}