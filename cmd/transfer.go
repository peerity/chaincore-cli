package cmd

import (
	"github.com/spf13/cobra"
	"log"
	"fmt"
	"encoding/hex"
	"gitlab.com/peerity/chaincore/types"
	"strings"
	"encoding/json"
	rpcjson "github.com/gorilla/rpc/json"
)

type TransferFlagsType struct {
	PrivKey 	string
	Sender		string
	Receiver	string
	Amount 		int64
}

var transferflags TransferFlagsType

func init() {
	transferCmd.Flags().StringVarP(&transferflags.PrivKey, "PrivKey", "", "", "Private Key in Hex String")
	transferCmd.Flags().StringVarP(&transferflags.Sender, "Sender", "", "", "Sender")
	transferCmd.Flags().StringVarP(&transferflags.Receiver, "Receiver", "", "", "Receiver")
	transferCmd.Flags().Int64VarP(&transferflags.Amount, "Amount", "", 0, "Amount")
	RootCmd.AddCommand(transferCmd)
}

var transferCmd = &cobra.Command{
	Use:   "transfer",
	Short: "SendTokenOperation",
	Long:  `SendTokenOperation`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(transferflags.PrivKey) <= 0 {
			fmt.Println("Error: PrivKey is required")
			return
		}

		if len(transferflags.Sender) <= 0 {
			fmt.Println("Error: Sender Username is required")
			return
		}

		if len(transferflags.Receiver) <= 0 {
			fmt.Println("Error: Receiver Username is required")
			return
		}

		if (transferflags.Amount <= 0) {
			fmt.Println("Error: Amount must be > 0")
			return
		}

		prikey_bytes, _ := hex.DecodeString(transferflags.PrivKey);
		prikey, _ := types.GetPrivateKeyFromBytes(prikey_bytes)

		var tx types.Transaction
		tx.Fee = 0
		tx.Username = transferflags.Sender

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		// AccountCreateOperation
		tx.Pubkey = prikey.PubKey().KeyString()

		tx.Type = "SendTokenOperation"
		opt := types.SendTokenOperation {
			ToUsername: transferflags.Receiver,
			Amount: transferflags.Amount,
		}

		////////////////////////////////////////////////////////////////////////////////////////////////////////////
		opt_arr, err := json.Marshal(opt)
		if (err != nil) {
			panic(err.Error())
		}
		//log.Println("opt:", string(opt_arr))
		opt_str := strings.ToUpper(hex.EncodeToString(opt_arr))

		tx.Operation = opt_str

		// signature
		sign := prikey.Sign([]byte(opt_str))
		sign_str := strings.ToUpper(hex.EncodeToString(sign.Bytes()))
		sign_str = sign_str[2:len(sign_str)]

		tx.Signature = sign_str

		// encode to json
		//json_bytes, _ := json.Marshal(tx)
		//log.Println("tx=", string(json_bytes))

		// json-rpc
		buf, _ := rpcjson.EncodeClientRequest("TransactionService.Broadcast", tx)
		log.Println("jsonrpc= ", string(buf[:]) )
	},
}